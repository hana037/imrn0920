console.log('--Nomor 1--');
console.log('Release 0')
class Animal {
    constructor(animalname, legs){
        this._name = animalname;
        this._legs = 4;
        this.cold_blooded = false;
    }
    get name() {
        return this._name;
    }
    
    get legs() {
      return this._legs;
  }
    set legs(x) {
      this._legs = x;
    
  }
} 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log('\n\nRelease 1')
class Ape extends Animal{
    constructor(animalname,x){
        super(animalname)
        this._legs=x
    }
    static legs(){
        return 2;
    }
    yell(){
        return console.log('Auooo');
    }
}

class Frog  extends Animal{
    constructor(animalname){
        super(animalname)
        
    }
    jump(){
        return console.log('Hop Hop');
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
console.log(sungokong.legs)
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log('\n\n--Nomor 2--');

class Clock {
    constructor({ template }) {
      this.template = template;
    }
  
    render() {
      let date = new Date();
  
      let hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      let mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      let secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop() {
      clearInterval(this.timer);
    }
  
    start() {
      this.render();
      this.timer = setInterval(() => this.render(), 1000);
    }
  }

var clock = new Clock({template: 'h:m:s'});
clock.start(); 