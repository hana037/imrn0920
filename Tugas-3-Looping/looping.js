//Soal1
console.log('LOOPING PERTAMA');
var number = 2;

while (number < 21) {
    console.log(number+' - I love coding');
    number += 2;
}

console.log('LOOPING KEDUA');
number = number-2;
while (number > 0) {
    console.log(number+' - I will become a mobile developer');
    number -= 2;
}

// Soal 2
console.log(' ');
console.log('OUTPUT');
for(var angka = 1; angka < 21; angka++) {
    if (angka % 2 == 0) {
        console.log(angka+' - Berkualitas');
    } else if (angka % 3 == 0) {
        console.log(angka+' - I Love Coding');
    } else{
        console.log(angka+' - Santai');
    }
  } 

//Soal 3
console.log(' ')
var inputpanjang=8; //bisa diganti sesuai keinginan
var inputlebar=4; //bisa diganti sesuai keinginan
var symbol='#';
var hasil='';

for(var panjang = 1; panjang <= inputpanjang; panjang++) {
    hasil += symbol;
  }
for(var lebar = 1; lebar <= inputlebar; lebar++) {
    console.log(hasil);
    }

//Soal 4
console.log(' ');
var inptinggi = 7; //bisa diganti sesuai keinginan
var tangga=''; 

for (var tinggi = 1; tinggi <= inptinggi; tinggi++) {
    tangga+=symbol;
    console.log(tangga)   
}

//Soal 5
console.log(' ');
var size=8; //bisa diganti sesuai keinginan
var catur='';

for (var row = 0; row < size; row++) {
    for (var col = 0; col < size; col++) {
        if((row+col)%2 == 0){
            catur += ' ';
        }else{
            catur += symbol;
        }
    }
    catur += '\n';
}
console.log(catur);