//Soal 1
console.log('--Nomor 1--');

function arrayToObject(dataArray) {
    var now = new Date();
    var thisYear = now.getFullYear();
    var data={};
    for (var i = 0; i < dataArray.length; i++) {
        var tahunlahir=dataArray[i][3];
        if (dataArray[i].length == 0) {
            console.log('error')
        }else if (thisYear>= tahunlahir) {
            var umur = thisYear - tahunlahir;
        }else{
            var umur = 'Invalid birth year'
        }
        var no = i+1;
        var data ={
            firstName:dataArray[i][0], lastName:dataArray[i][1], gender:dataArray[i][2], age:umur
        }
        hasil = no+'. '+data.firstName+' '+data.lastName+' : ';
        process.stdout.write(hasil)
        console.log(data)
    }
}
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people);

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
arrayToObject(people2);
arrayToObject([]);

//Soal 2
console.log('\n\n--Nomor 2--');

function shoppingTime(IDMember, money) {
    var hasil =''
    if (typeof IDMember == 'undefined' || IDMember.trim() =='') {
        hasil = 'Mohon maaf, toko X hanya berlaku untuk member saja'
    }else if (money < 50000) {
        hasil = 'Mohon maaf, uang tidak cukup'
    }else{
        var listbeli=[];
        var uangawal=money;
        var StacatuPurchased = false;
        var zoroPurchased = false;
        var hnnPurchased = false;
        var unikloohPurchased = false;
        var cashingPurchased = false;
        while (money>=50000) {
            if (money>=1500000 && StacatuPurchased == false) {
                listbeli.push('Sepatu Stacatu');
                money -= 1500000;
                StacatuPurchased = true;
            }else if (money>=500000 && zoroPurchased == false) {
                listbeli.push('Baju Zoro');
                money -= 500000;
                zoroPurchased = true;
            }else if (money>=250000 && hnnPurchased == false) {
                listbeli.push('Baju H&N');
                money -= 250000;
                hnnPurchased = true;
            }else if (money>=175000 && unikloohPurchased == false) {
                listbeli.push('Sweater Uniklooh');
                money -= 175000;
                unikloohPurchased = true
            }else if (money >=50000 && cashingPurchased == false) {
                listbeli.push('Casing Handphone');
                money -= 50000;
                cashingPurchased = true;
            }else{
                break;
            }
        }
        hasil={memberID:IDMember, money:uangawal, listPurchased:listbeli, changeMoney:money}
    } return hasil
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

//Soal 3
console.log('\n\n--Nomor 3--');
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var angkot = [{},{}];
    if (arrPenumpang.length==0) {
        angkot=[]
    }else {
        for (var i=0; i<arrPenumpang.length; i++) {
            angkot[i].penumpang = arrPenumpang[i][0];
            angkot[i].naikDari = arrPenumpang[i][1];
            angkot[i].tujuan = arrPenumpang[i][2];
            var awal = rute.indexOf(arrPenumpang[i][1]);
            var akhir = rute.indexOf(arrPenumpang[i][2]);
            var bayar = Math.abs(akhir-awal)*2000;
            angkot[i].bayar = bayar; 
        }    
    }
    
    return angkot;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));