import React from 'react';
import { 
  View, 
  Text, 
  ScrollView, 
  TextInput, 
  TouchableOpacity, 
  StyleSheet ,
  Image,
  FlatList,
  KeyboardAvoidingView
} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import data from './skillData.json';

function Item ({ item }) {
    return (
        <View>
        <TouchableOpacity style={styles.skill}>
            <View style={styles.kotakSkill}>
                <View style={styles.iconSkill}>
                    <MaterialCommunityIcons name={item.iconName} size={85} solid color="#003366" />
                </View>
                <View style={{ justifyContent: 'center', marginLeft: 10}}>
                    <View>
                        <Text style={styles.namaSkill}>{item.skillName}</Text>
                    </View>
                    <View>
                        <Text style={styles.kategori}>{item.categoryName}</Text> 
                    </View>
                    <View>
                        <Text style={styles.persen}>{item.percentageProgress}</Text>
                    </View>
                </View>
                <View style={styles.panah}>
                    <MaterialCommunityIcons name={'chevron-right'} size={100} solid color="#003366" />
                </View>
            </View>
        </TouchableOpacity>
    </View>
    );
}

export default class App extends React.Component {
    state = data;
    render(){
      return (
        <KeyboardAvoidingView
          behavior = {Platform.OS == 'ios' ? "padding": 'height'}
          style={styles.container}>
  
           <View style={styles.container}>
              <Image source={require('./images/logo.png')} style={{width:190, height:50, marginTop: 35, marginLeft: 160, marginBottom: 10, alignSelf:'flex-end'}}/>
                    
                    <View style={styles.akun}>
                        <View>
                            <FontAwesome5 name={'user-circle'}
                                size={35} solid
                                color="#3EC6FF"
                            />
                        </View>
                        <View style={{ justifyContent: 'center', marginLeft: 10}}>
                            <Text style={styles.teksHai}>Hai,</Text>
                            <Text style={styles.teksNama}>Hana</Text>
                        </View>
                    </View>

                    <View style={styles.SkillNama}>
                        <Text style={styles.SkillTeks}>SKILL</Text>
                        <View style={styles.garis}></View>
                    </View>

                    <View style={styles.kbSkill}>
                        <TouchableOpacity style={styles.kategoryBt} >
                            <Text style={styles.textbt}>Library / Framework</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.kategoryBt} >
                            <Text style={styles.textbt}>Bahasa Pemrograman</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.kategoryBt} >
                            <Text style={styles.textbt}>Teknologi</Text>
                        </TouchableOpacity>
                    </View>       
                <FlatList
                style={{flex:1}}
                data={this.state.items}
                renderItem={({ item }) => <Item item={item}/>}
                keyExtractor={item => item.id}
                />
            </View>
        </KeyboardAvoidingView>

      );
    }
  }
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingLeft: 5,
        paddingRight: 5
    },
    akun: {
        height: 35,
        flexDirection: 'row',
        marginBottom:2

    },
    teksHai: {
        fontSize: 14
    },
    teksNama:{
        fontSize: 18,
        color: '#003366'
    },
    SkillNama: {
        marginTop: 25,
    },
    SkillTeks: { 
        fontSize: 38,
        color:"#003366"
    },
    garis: {
        borderTopWidth: 6,
        borderTopColor: '#3EC6FF',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    kbSkill: {
        flexDirection: 'row',
        marginBottom:2

    },
    kategoryBt: {
        backgroundColor: "#B4E9FF",
        padding: 10,
        borderRadius: 10,
        marginBottom: 10,
        marginTop:10,
        marginRight:5,
        width: 110,
    },
    textbt: {
        color: '#003366',
        textAlign: "center",
        fontSize: 11,
        fontWeight: "bold",
        textAlignVertical: "center"
    },
    skill: {
        backgroundColor: "#B4E9FF",
        padding: 10,
        borderRadius: 8,
        marginBottom: 10,
        marginTop:10,
        marginRight:5,
        width: 330,
        height: 130,
    },
    iconSkill: {
        marginTop: 6,
    },
    kotakSkill: {
        height: 35,
        flexDirection: 'row',
        marginBottom:2
    },
    namaSkill: {
        color: '#003366',
        fontSize: 24,
        fontWeight: '700',
        marginTop: 80,
    },
    kategori: {
        color: '#3EC6FF',
        fontSize: 14,
        fontWeight: "700"
    },
    persen: {
        color: "white",
        fontSize: 48,
        fontWeight: "700",
        marginLeft: 20,
        textAlign: "right"
    },
    panah: {
        marginTop: 6,
        marginLeft: -10,
    },
  });