import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const Skill = ({data}) => {
  return (
    <View style={styles.container}>
      <View style={{width: '30%', alignItems: 'center', justifyContent: 'center'}}>
        <Icon name={data.iconName} size={70} color="#003366" />
      </View>
      <View style={{width: '50%'}}>
        <Text style={{fontSize: 24, fontWeight: 'bold', color: "#003366"}}>{data.skillName}</Text>
        <Text style={{fontSize: 16, fontWeight: 'bold', color: "#3EC6FF"}}>{data.categoryName}</Text>
        <View style={{alignItems: 'flex-end'}}>
          <Text style={{fontSize: 48, fontWeight: 'bold', color: "#FFFFFF", }}>{data.percentageProgress}</Text>
        </View>
      </View>
      <View style={{alignItems: 'center', justifyContent: 'center', width: '20%'}}>
        <Icon name="greater-than" size={45} color="#003366" />
      </View>
    </View>
  );
};

export default Skill;

const styles = StyleSheet.create({
  container: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.25,
    shadowRadius: 5.46,
    elevation: 9,
    height: 129, borderRadius: 8, marginLeft: 14, marginRight: 18, marginBottom: 8, backgroundColor: '#B4E9FF', flexDirection: 'row',
  },
});
