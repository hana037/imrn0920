import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const AddScreen = () => {
  return (
    <View style={styles.container}>
      <Text>Halaman Tambah</Text>
    </View>
  );
};

export default AddScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
