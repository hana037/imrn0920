import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const ProjectScreen = () => {
  return (
    <View style={styles.container}>
      <Text>Halaman Proyek</Text>
    </View>
  );
};

export default ProjectScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
