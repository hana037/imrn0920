import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';

import LoginScreen from './LoginScreen';
import About from './AboutScreen';
import SkillScreen from './SkillScreen';
import Project from './ProjectScreen';
import AddScreen from './AddScreen';
import Register from './Register';

const LoginStack = createStackNavigator();
const Draw = createDrawerNavigator();
const Tabs = createBottomTabNavigator();
const RegisterStack = createStackNavigator();

const LoginStackScreen = () => (
  <LoginStack.Navigator>
    <LoginStack.Screen name="Login" component={LoginScreen} />
    <LoginStack.Screen name="Register" component={Register} />
  </LoginStack.Navigator>
);

const RegisterStackScreen =() => (
  <RegisterStack.Navigator>
    <RegisterStack.Screen name="Register" component={Register} />
  </RegisterStack.Navigator>
)

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="SkillScreen" component={SkillScreen} />
    <Tabs.Screen name="ProjectScreen" component={Project} />
    <Tabs.Screen name="AddScreen" component={AddScreen} />
  </Tabs.Navigator>
);

export default () => (
  <NavigationContainer>
    <Draw.Navigator>
      <Draw.Screen name="Login" component={LoginStackScreen} />
      <Draw.Screen name="Tabs" component={TabsScreen} />
      <Draw.Screen name="AboutScreen" component={About} />
    </Draw.Navigator>
  </NavigationContainer>
);
