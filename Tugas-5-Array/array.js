// Soal 1
console.log('Nomor 1');
function range(startnum, finishNum) {
    var list =[];
    if (startnum<finishNum) {
        while (startnum <= finishNum) {
            list.push(startnum);
            startnum ++ ;  
        }return list;
    }else if (startnum>finishNum) {
        while (startnum >= finishNum) {
            list.push(startnum);
            startnum -- ;   
        }return list;
    }else {
        return -1;
    }
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54, 50));
console.log(range());

//Soal 2
console.log('\nNomor 2');
function rangeWithStep(startnum, finishNum, step) {
    var list =[];
    if (startnum<finishNum) {
        while (startnum <= finishNum) {
            list.push(startnum);
            startnum += step ;  
        }return list;
    }else if (startnum>finishNum) {
        while (startnum >= finishNum) {
            list.push(startnum);
            startnum -= step ;   
        }return list;
    }else {
        return list;
    }
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

// Soal 3
console.log('\nNomor 3');
function sum(startnum, finishNum, step=1) {
    var hasil = 0;
    if (startnum<finishNum) {
        while (startnum <= finishNum) {
            hasil+=startnum;
            startnum += step ;  
        }return hasil;
    }else if (startnum>finishNum) {
        while (startnum >= finishNum) {
            hasil+=startnum;
            startnum -= step ;   
        }return hasil;
    }else if (typeof startnum == 'undefined' && typeof finishNum == 'undefined') {
        return 0
    }else if(typeof finishNum == 'undefined'){
        return startnum;
    }else {
        return finishNum;
    }
}
console.log(sum(1,10));
console.log(sum(5, 50, 2));
console.log(sum(15,10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());


//Nomor 4
console.log('\nNomor 4');
function dataHandling(data) {
    for (var line = 0; line < data.length; line++) {
        console.log('Nomor ID: '+data[line][0]);
        console.log('Nama Lengkap: '+data[line][1]);
        console.log('TTL: '+data[line][2]+' '+data[line][3]);
        console.log('Hobi: '+data[line][4]+'\n');
        
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] ;

dataHandling(input);

//Nomor 5
console.log('Nomor 5');
function balikKata(kata) {
    hasil='';
    for (var i = kata.length -1; i >=0; i--) {
        hasil += kata[i];
    }
    return hasil;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

//Nomor 6

console.log('\nNomor 6');

function dataHandling2(input) {
    input[1] = input[1]+'Elsharawy'
    input[2] = 'Provinsi '+ input[2];
    input.splice(-1,1,"Pria", "SMA Internasional Metro");
    console.log(input);
    var ttl = input[3].split('/');
    var months={'1':'Januari','2':'Februari','3':'Maret','4':'April','5':'Mei','6':'Juni','7':'Juli','8':'Agustus','9':'September','10':'Oktober','11':'November','12':'Desember'}
    var bulan=months[parseInt(ttl[1])];
    console.log(bulan);
    var sortedttl = ttl.sort(function (value1, value2) { return value2 - value1 });
    console.log(sortedttl);
    var jointtl = input[3].split('/').join('-');
    console.log(jointtl);
    var slicename = input[1].slice(0,15);
    console.log(slicename);
}

var input2 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

dataHandling2(input2);