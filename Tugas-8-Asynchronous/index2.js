var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
var i=0;
function buku(timelimit,books){
    if (books[i] != undefined){
        readBooksPromise(timelimit, books[i])
            .then(function (fulfilled) {
                i++;
                buku(fulfilled,books);        
            })
            .catch(function (error) {
                
            })
    }
}

var timelimit=1000;
buku(timelimit, books);

async function asyncall() {
    let t = 10000
    for (let i = 0; i < books.length; i++) {
        t= await readBooksPromise(t,books[i]).then(function(SisaWaktu){
            return SisaWaktu;
        })
        .catch(function(SisaWaktu){
            return SisaWaktu;
        })
        
    }
    console.log('selesai')
}