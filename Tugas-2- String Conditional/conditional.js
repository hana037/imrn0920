//Soal 1

var nama = 'John'
var peran = ''
console.log('Nomor 1')
if (nama.trim() == '') {
    console.log('Nama harus diisi!')
}else if(peran.trim() ==''){
    console.log('Halo '+nama+', Pilih peranmu untuk memulai game!')
}else{
    console.log('Selamat datang di Dunia Werewolf, '+nama)
    if(peran.toLowerCase() == 'penyihir'){
        console.log('Halo '+peran+' '+nama+', kamu dapat melihat siapa yang menjadi werewolf!')
    } else if (peran.toLowerCase() == 'guard'){
        console.log('Halo '+peran+' '+nama+', kamu akan membantu melindungi temanmu dari serangan werewolf.')
    } else if (peran.toLowerCase() == 'werewolf'){
        console.log('Halo '+peran+' '+nama+', Kamu akan memakan mangsa setiap malam!')
    }else{
        console.log('Halo '+nama+', Maaf hanya ada 3 peran: Penyihir, Guard, Werewolf. Coba pilih kembali!')
    }
}

// Soal 2
var tanggal = 30; 
var bulan = 9; 
var tahun = 2020;
console.log('Nomor 2')
if (tanggal<1 || tanggal>31 || !Number.isInteger(tanggal)){
    console.log('Format tanggal harus diisi dengan angka bulat antara 1-31')
} else if(bulan<1 || bulan>12 || !Number.isInteger(bulan)){
    console.log('Format bulan harus diisi dengan angka bulat antara 1-12')
} else if (tahun<1900 || tahun>2200 || !Number.isInteger(tahun)){
    console.log('Format tahun harus diisi dengan angka bulat antara 1900-2200')
}else{
    switch(bulan){
        case 1:
            bulan = 'Januari'; break;
        case 2:
            bulan = 'Februari'; break;
        case 3:
            bulan = 'Maret'; break;
        case 4:
            bulan = 'April'; break;
        case 5:
            bulan = 'Mei'; break;
        case 6:
            bulan = 'Juni'; break;
        case 7:
            bulan = 'Juli'; break;
        case 8:
            bulan = 'Agustus'; break;
        case 9:
            bulan = 'September'; break;
        case 10:
            bulan = 'Oktober'; break;
        case 11:
            bulan = 'November'; break;
        case 12:
            bulan = 'Desember'; break;
    } console.log([tanggal,bulan,tahun].join(' '));
}